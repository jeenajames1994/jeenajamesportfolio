import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import './App.css';
import PrivateRoute from './privateRoute';
import userProfile from './pages/profile';
import  userSkill from './pages/skill';
import  userWork from './pages/work';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
        <Redirect exact from="/" to="/profile" />
          <Route
            path="/profile"
            exact
            render={() => (
              <PrivateRoute component={userProfile} action="profile" />
            )}
          />
          <Route
            path="/skill"
            exact
            render={() => (
              <PrivateRoute component={userSkill} action="skill" />
            )}
          />    
          <Route
            path="/work"
            exact
            render={() => (
              <PrivateRoute component={userWork} action="work" />
            )}
          />         
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;