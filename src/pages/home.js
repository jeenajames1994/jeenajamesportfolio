import React, { useState } from 'react';

import './skill';
import './profile';
import './work';
import '../styles/home.css'
import pic from '../assets/jeena.jpg'


const menuArray = [{ name: 'Profile', link: 'profile' },
{ name: 'Skill', link: 'skill'},
{ name: 'Work', link: 'work'}
];

export default function Home() {

    return (
        <div className='homeWrapper'>
            <div className='imageWrapper'>
                <img src={pic} className='profileImage' alt="image" />
                <div className='list'>
                    <ul className='listStyle'>
                        {menuArray.map((item, index) => <li key={index}>
                            <a onClick={(event) => {
                                event.preventDefault();
                                window.location = `${menuArray[index].link}`;
                                }}
                                className={window.location.pathname.includes(item.link) && 'selectedStyle' || ''}
                                href={item.link}>{item.name}
                            </a>
                        </li>)}
                    </ul>
                </div>
            </div>
        </div>
    );
}
