import React from 'react';

import { works } from './constant';
import Home from './home';
import '../styles/work.css'

export default function work() {
    return (
        <div className='workWrapper'>
            <Home />
            <div className='headerStyle'><h3>Work</h3></div>
            <div className='detailContainer'>
                {works.map((item, index) => <div key={index}
                    className={index % 2 === 0 ? 'timeLineContainerRight' : 'timeLineContainerLeft'}>
                    <div className='timeLineDetails'><p>{item.name}</p></div>
                    <div className={index % 2 === 0 ? 'dateRightDetails' : 'dateLeftDetails'}>{item.year}</div>
                </div>)}
            </div>
        </div>
    );
}
