import React, { useEffect, useRef } from 'react';
import { useLocation } from 'react-router-dom';

import { skillSet, backendSkill, awsSkill } from './constant';
import Home from './home';
import Card from '../components/card'
import '../styles/skill.css'


const Skill = () => {
    const location = useLocation();
    const { skillType } = location.state || '';
    const sectionRefs = useRef({ frontend: null, backend: null, cloud: null });

    useEffect(() => {
        if (skillType && sectionRefs.current[skillType]) {
            sectionRefs.current[skillType].scrollIntoView({ behavior: 'smooth' });
        }
    }, [skillType]);

    return (
        <>
            <Home />
            <div className='headerStyle'><h3>My Skill Set</h3></div>
            <div className='mySkillSet'>
                <div className='skillCardContainer' ref={ref => sectionRefs.current['frontend'] = ref} >
                    <div><h3>FRONTEND</h3></div>
                    <Card skillSet={skillSet} skill="frontend" angle="-80" />
                </div>
                <div className='skillCardContainer' ref={ref => sectionRefs.current['backend'] = ref} >
                    <div><h3>BACKEND</h3></div>
                    <Card skillSet={backendSkill} skill="backend" angle="-20" />
                </div>
                <div className='skillCardContainer' ref={ref => sectionRefs.current['cloud'] = ref} >
                    <div><h3>CLOUD</h3></div>
                    <Card skillSet={awsSkill} skill="cloud" angle="0" />
                </div>
            </div>
        </>
    );
}
export default Skill;
