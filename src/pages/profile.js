import React from 'react';
import { useHistory } from 'react-router-dom';

import Home from './home';
import backend from '../assets/backend.jpg'
import cloud from '../assets/cloud.jpg'
import frontSkill from '../assets/frontSkill.jpg'
import '../styles/profile.css';

const Profile = () => {

    const history = useHistory();

    const handleClick = (skill) => {
        history.push({
          pathname: '/skill',
          state: {skillType: skill } 
        });
      };

    return (
        <div className='profileWrapper'>
            <Home />
            <div className='headerStyle'><h3>Hi, I'm Jeena👋</h3></div>
            <div className='cardWrapper'>
                <div className='cardStyle' onClick={() => handleClick("frontend")}>
                    <h1>FRONTEND SKILLS</h1>
                    <img src={frontSkill} className='frontStyle' alt="image" />
                </div>
                <div className='cardStyle' onClick={() => handleClick("backend")}>
                    <h1>BACKEND SKILLS</h1>
                    <img src={backend} className='frontStyle' alt="image" />
                </div>
                <div className='cardStyle' onClick={() => handleClick("cloud")}>
                    <h1>CLOUD SKILLS</h1>
                    <img src={cloud} className='frontStyle' alt="image" />
                </div>
            </div>
        </div>
    );
}

export default Profile;