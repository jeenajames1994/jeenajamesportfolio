import html from '../assets/html.jpg';
import css from '../assets/css.jpg';
import javascript from '../assets/javascript.png';
import reactIcon from '../assets/react.jpg';
import reduxIcon from '../assets/redux.jpg';
import sagaIcon from '../assets/saga.jpg';
import babel from '../assets/babel.jpg';
import jest from '../assets/jest.png';
import tailwind from '../assets/tailwind.png';
import nextjs from '../assets/nextjs.jpg';
import webpack from '../assets/webpack.jpg';
import typescript from '../assets/typescript.jpg';
import nodeIcon from '../assets/node.png'
import java from '../assets/java.png';
import sql from '../assets/sql.png';
import mongo from '../assets/mongodb.svg';
import aws from '../assets/aws.jpg';

export const skillSet = [
    { name: "HTML", asset: html, color: 'rgb(240, 88, 55)', desc: 'The HTML file is a text document containing markup language instructions that define the structure and content of a web page.', rating: 4.5 },
    { name: "CSS", asset: css, color: 'rgb(0, 123, 255)', desc: 'CSS (Cascading Style Sheets) is a language used to style the appearance of web pages, controlling elements colors, fonts, spacing, and layout.', rating: 4.5 },
    { name: "Javascript", asset: javascript, color: 'rgb(217, 172, 42)', desc: 'JavaScript is a high-level programming language commonly used for web development to add interactivity, dynamic behavior, and functionality to websites.', rating: 4.5 },
    { name: "React", asset: reactIcon, color: 'rgb(12, 53, 130)', desc: 'React is a JavaScript library for building user interfaces, commonly used for creating interactive and dynamic web applications with reusable components.', rating: 4.5 },
    { name: "Redux", asset: reduxIcon, color: 'rgb(36, 37, 39)', desc: 'Redux is a predictable state container for JavaScript applications, commonly used with React for managing application state in a centralized and predictable manner.', rating: 4.5 },
    { name: "Redux-Saga", asset: sagaIcon, color: 'rgb(74 85 148)', desc: 'Redux-saga is a library for managing side effects, such as asynchronous data fetching and complex state transitions, in Redux applications using ES6 generators.', rating: 4.5 },
    { name: "Babel", asset: babel, color: 'rgb(59, 30, 97)', desc: 'Babel is a JavaScript compiler that transforms ECMAScript 2015+ (ES6+) code into backward-compatible versions of JavaScript, allowing developers to use modern language features while ensuring compatibility with older browsers.', rating: 4.5 },
    { name: "Jest", asset: jest, color: 'rgb(8, 98, 173)', desc: 'Jest is a JavaScript testing framework built by Facebook, commonly used for unit and integration testing of JavaScript applications, including React components.', rating: 4 },
    { name: "Tailwind CSS", asset: tailwind, color: 'rgb(7, 38, 59)', desc: 'Tailwind CSS is a utility-first CSS framework that provides a set of pre-designed utility classes to rapidly build custom user interfaces with minimal CSS code.', rating: 4 },
    { name: "Webpack", asset: webpack, color: 'rgb(151 14 69)', desc: 'Webpack is a module bundler for JavaScript applications, commonly used to bundle and manage assets, such as JavaScript files, CSS files, and images, for deployment in web applications.', rating: 4 },
    { name: "Typescript", asset: typescript, color: 'rgb(107 60 87)', desc: 'TypeScript is a superset of JavaScript that adds static typing and other features to enable robust development of large-scale JavaScript applications.', rating: 4.5 },
    { name: "Next js", asset: nextjs, color: 'rgb(14 72 23)', desc: 'Next.js is a React framework for building server-side rendered and statically generated web applications, providing features like automatic code splitting, routing, and server-side rendering out of the box.', rating: 3 },
]


export const backendSkill = [
    { name: "Node.js", asset: nodeIcon, color: 'rgb(11 120 55)', desc: 'Node.js is a JavaScript runtime built on Chromes V8 JavaScript engine, designed for scalable network applications.', rating: 3 },
    { name: "Java", asset: java, color: 'rgb(96 19 49)', desc: 'Java is a high-level, object-oriented programming language known for its platform independence and wide range of applications, from enterprise software development to mobile app development.', rating: 3 },
    { name: "SQL", asset: sql, color: 'rgb(0 88 107)', desc: 'SQL (Structured Query Language) is a domain-specific language used for managing and manipulating relational databases, allowing users to create, retrieve, update, and delete data stored in a database management system.', rating: 3.5 },
    { name: "MongoDB", asset: mongo, color: 'rgb(61 180 109)', desc: 'MongoDB is a popular NoSQL database management system known for its flexible document-oriented data model and scalability.', rating: 3.5 },
]

export const awsSkill = [
    { name: "AWS", asset: aws, color: 'rgb(190 140 7)', desc: 'Amazon Web Services (AWS) provides cloud computing services and solutions.', rating: 3 },
]

export const works = [
    { name: 'Completed MTECH in Computer science', year: 2023 },
    { name: 'Joined Accenture as Senior Software Engineer', year: 2021 },
    { name: 'Joined QBurst Technologies as a software Engineer', year: 2018 },
    { name: 'Completed BTECH in Information Technology', year: 2016 },
]