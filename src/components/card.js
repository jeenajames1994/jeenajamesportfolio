import React, { useEffect, useState } from 'react';
import Rating from '../components/rating';
import '../styles/card.css';

const Card = ({ skillSet, skill, angle = -80 }) => {
    const [isHovered, setIsHovered] = useState(false);
    const [selectedIndex, setIndex] = useState(-1);

    useEffect(() => {
        const elements = document.querySelectorAll(`.${skill}-skillSubWrapper`);
        const angleIncrement = 15;
        elements.forEach((element, index) => {
            const rotationAngle = index === 0 ? Number(angle) : Number(angle) + angleIncrement * (index);
            element.style.transform = `rotate(${rotationAngle}deg) translateY(${(selectedIndex === index && isHovered) ? -200 : -140}px)`;
        });
    }, [isHovered, selectedIndex]);

    const handleMouseOver = (index) => {
        setIndex(index)
        setIsHovered(true);
    };

    const handleMouseLeave = () => {
        setIsHovered(false);
        setIndex(-1)
    };

    return (
        <div className='skillSetWrapper'>
            <div className='skillContainer'>
                <ul className='skillWapper' >
                    {skillSet.map((item, index) =>
                        <li id={skill} key={index} className={`${skill}-skillSubWrapper`}
                            style={{ background: item.color }} >
                            <div className='nameWrapper' onMouseOver={() => handleMouseOver(index)}
                                onMouseLeave={handleMouseLeave}>
                                <img src={item.asset} alt='' height='30px' width='30px' />
                                <div className='nameStyle'>{item.name}</div>
                            </div>
                            <div className='iconWrapper'><img src={item.asset} alt='' height='30px' width='30px' /></div>
                            <div className='iconContainer'>
                                <div className='lastName'>{item.name}</div>
                                <img src={item.asset} alt='' height='30px' width='30px' />
                            </div>
                        </li>
                    )}
                </ul>
            </div>
            <div>
                {skill == 'frontend' && selectedIndex == -1 ? <div>
                    <div>Hi, I'm Jeena James👋.</div>
                    I am a software engineer.
                    These are my skill set.
                </div> :
                    (selectedIndex != -1) && (<div className='desWrapper'>
                        <div className='ratingStyle'> My Rating: <Rating value={skillSet[selectedIndex].rating} max={5} /> </div>
                        <div>{skillSet[selectedIndex].desc} </div>
                    </div>)}
            </div>
        </div>
    )
};

export default Card;