import React from 'react';
import { FaStar, FaRegStar, FaStarHalfAlt } from 'react-icons/fa'; 

const Rating = ({ value, max }) => {
    const stars = [];

    for (let i = 1; i <= max; i++) {
      if (i <= value) {
        stars.push(<FaStar key={i} color="gold" />);
      } else if (i - 0.5 === value) {
        stars.push(<FaStarHalfAlt key={i} color="gold" />);
      } else {
        stars.push(<FaRegStar key={i} color="gray" />);
      }
    }
  
    return <div>{stars}</div>;
};

export default Rating;